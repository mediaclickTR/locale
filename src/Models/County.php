<?php


namespace Mediapress\Locale\Models;
use Illuminate\Database\Eloquent\Model;



class County extends Model
{
    protected $connection = 'mediapress_locale_module';
    protected $table = 'counties';
    protected $fillable = ['province_id','name'];
    public $timestamps = false;

    public function province()
    {
        return $this->belongsTo(Province::class,'province_id');
    }

    public function villages()
    {
        return $this->hasMany(Village::class, 'ilce_id');
    }
}
