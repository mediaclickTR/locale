<?php

namespace Mediapress\Locale;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;


class LocaleServiceProvider extends ServiceProvider
{
    public const LOCALE = "Locale";
    public const RESOURCES = "resources";
    protected $module_name = self::LOCALE;
    public const CONFIG = 'Config';
    public const ACTIONS_PHP = 'actions.php';
    public const PANEL = 'panel';
    protected $namespace = 'Mediapress\Locale';

    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . self::RESOURCES . DIRECTORY_SEPARATOR . 'lang' . DIRECTORY_SEPARATOR . 'panel' ,$this->module_name . 'PANEL');
        $this->publishActions(__DIR__);
        $files = $this->app['files']->files(__DIR__.'/Config');
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        $this->app['config']->set("database.connections.mediapress_locale_module", ['driver' => 'sqlite' , 'database' => __DIR__ . '/Database/Locale.db', 'prefix' => '']);
        $loader = AliasLoader::getInstance();
        $loader->alias(self::LOCALE,Locale::class);
        app()->bind(self::LOCALE,function ()
        {
            return new \Mediapress\Locale\Locale;
        });
    }

    protected function mergeConfig($path, $key)
    {
        $config = config($key);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        config([$key=>$config]);
    }

    protected function publishActions($dir){
        $actions_config = $dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . self::ACTIONS_PHP;
        if(is_file($actions_config)){
            $this->publishes([
                $dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . self::ACTIONS_PHP => config_path(strtolower($this->module_name).'_module_actions.php')
            ]);
//        $this->publishes([__DIR__ . DIRECTORY_SEPARATOR . 'Database' => database_path()], $this->module_name . 'Database');
            $this->mergeConfigFrom($dir . DIRECTORY_SEPARATOR . self::CONFIG . DIRECTORY_SEPARATOR . self::ACTIONS_PHP, strtolower($this->module_name).'_module_actions');
        }
    }
    protected function getConfigBasename($file)
    {
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($file));
    }


}
